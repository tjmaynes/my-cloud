#!/bin/bash

set -e

NEXTCLOUD_HOST=$1
NEXTCLOUD_PASSWORD=$2
NEXTCLOUD_DB_USER=nextcloud
NEXTCLOUD_DB_NAME=nextcloud

function check_requirements() {
  if [[ -z "$(command -v helm)" ]]; then
    echo "Please install 'helm' before running this script."
    exit 1
  elif [[ -z "$(command -v jq)" ]]; then
    echo "Please install 'jq' before running this script."
    exit 1
  elif [[ -z "$NEXTCLOUD_HOST" ]]; then
    echo "Please provide a host for Nextcloud"
    exit 1
  elif [[ -z "$NEXTCLOUD_PASSWORD" ]]; then
    echo "Please provide a password for Nextcloud."
    exit 1
  fi
}

function main() {
  check_requirements

  # helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo add nextcloud https://nextcloud.github.io/helm/
  helm repo update

  kubectl apply -f services/nextcloud/persistence.yml

  # helm install nextcloud-db bitnami/postgresql --atomic \
  #   --set fullnameOverride="nextcloud-db" \
  #   --set image.tag="11.13.0-debian-10-r33" \
  #   --set postgresqlDatabase=$NEXTCLOUD_DB_NAME \
  #   --set postgresqlUsername=$NEXTCLOUD_DB_USER \
  #   --set persistence.enabled=true \
  #   --set persistence.existingClaim=nextcloud-db-pvc

    # kubectl get secret nextcloud-db -o json | jq --arg user "$(echo -n $NEXTCLOUD_DB_NAME | base64)" '.data["db-username"]=$user' | kubectl apply -f -

  CUSTOM_CONFIG=$(cat <<-END
    <?php
    $CONFIG = array (
      'overwriteprotocol' => 'https',
      'overwrite.cli.url' => '$NEXTCLOUD_HOST',
      'filelocking.enabled' => 'true',
      'loglevel' => '2',
      'enable_previews' => false
    );
END
  )

  helm upgrade --install nextcloud nextcloud/nextcloud --atomic \
    --set image.tag=21.0.4 \
    --set nextcloud.password=$NEXTCLOUD_PASSWORD \
    --set internalDatabase.enabled=false \
    --set mariadb.enabled=true \
    --set mariadb.db.name=nextcloud \
    --set mariadb.db.password=password \
    --set mariadb.db.user=nextcloud \
    --set mariadb.rootUser.password=changeme \
    --set mariadb.master.persistence.enabled=true \
    --set mariadb.master.persistence.accessMode=ReadWriteOnce \
    --set mariadb.master.persistence.size=8Gi
    --set nextcloud.configs.custom.config.php="$CUSTOM_CONFIG" 
    # --set externalDatabase.type=postgresql \
    # --set externalDatabase.host=nextcloud-db:5432 \
    # --set externalDatabase.database=nextcloud \
    # --set externalDatabase.existingSecret.enabled=true \
    # --set externalDatabase.existingSecret.secretName=nextcloud-db \
    # --set externalDatabase.existingSecret.usernameKey=db-username \
    # --set externalDatabase.existingSecret.passwordKey=postgresql-password \
    # --set persistence.enabled=true \
    # --set persistence.existingClaim=nextcloud-pvc \
    # --set persistence.accessMode=ReadWriteOnce
    # --set nextcloud.host=$NEXTCLOUD_HOST \
    # --set service.type=NodePort \
    # --set service.nodePort=32123 \
    # --set cronjob.enabled=true \
    # --set cronjob.curlInsecure=false \
    # --set cronjob.failedJobsHistoryLimit=5 \
    # --set cronjob.schedule="*/5 * * * *" \
    # --set cronjob.successfulJobsHistoryLimit=2 \
    #--set metrics.enabled=true \
    #--set metrics.https=false \
    #--set metics.image.pullPolicy=IfNotPresent \
    #--set metrics.image.repostiory=xperimental/nextcloud-exporter \
    #--set metrics.image.tag=0.4.0 \
    #--set metrics.replicaCount=1 \
    #--set metrics.service.type=ClusterIP \
    #--set metrics.timeout=5s
}

main
