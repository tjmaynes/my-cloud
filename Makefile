start:
	./services/nextcloud/nextcloud.sh "127.0.0.1" "password"

stop:
	helm delete prometheus || true
	helm delete nextcloud-db || true
	helm delete nextcloud || true

open_nextcloud:
	kubectl port-forward $(shell kubectl get pods -l "app.kubernetes.io/name=nextcloud" -o jsonpath="{.items[0].metadata.name}") 8080:80