# My Cloud
> Running various open source services on Kubernetes.

## Requirements

- [GNU Make](https://www.gnu.org/software/make/)
- [Kubernetes](https://kubernetes.io/)

## Usage
To start the project, run the following command:
```bash
make start
```

To stop the project, run the following command:
```bash
make stop
```